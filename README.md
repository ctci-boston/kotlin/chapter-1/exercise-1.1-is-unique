# exercise-1.1-is-unique

Provides a Kotlin solution for implementing an algorithm to determine if a string has all unique characters.