import org.junit.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class IsUniqueTest {
    @Test
    fun `when the empty string is presented verify true`() {
        assertTrue(isUnique(""))
    }

    @Test
    fun `when a single character string is presented verify true`() {
        assertTrue(isUnique("a"))
    }

    @Test
    fun `when a two unique character string is presented verify true`() {
        assertTrue(isUnique("ab"))
    }

    @Test
    fun `when a two identical character string is presented verify false`() {
        assertFalse(isUnique("aa"))
    }
}