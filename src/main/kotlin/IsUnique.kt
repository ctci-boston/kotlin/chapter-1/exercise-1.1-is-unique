fun isUnique(s: String): Boolean {
    fun isUnique(): Boolean {
        val chars: CharArray = s.toCharArray()
        chars.sort()
        for (n in 0 .. chars.size - 2) { if (chars[n] == chars[n + 1]) return false }
        return true
    }

    return when {
        s.isBlank() || s.length == 1 -> true
        else -> isUnique()
    }
}
